from flask import Flask, request
from google.cloud import vision
from google.cloud import firestore
import json
import os
if os.path.exists('env.py'):
    import env

db = firestore.Client()
app = Flask(__name__)

app.secret_key = os.environ.get("SECRET_KEY")


@app.route('/detect_labels_uri', methods=['POST'])
def detect_labels_uri():
    """Log the request payload."""
    payload = request.get_data(as_text=True) or '(empty payload)'
    data = json.loads(payload)
    filename = data['filename']
    client = vision.ImageAnnotatorClient()
    image = vision.Image()
    image.source.image_uri = 'gs://ypa-image-upload.appspot.com/' + filename

    response = client.label_detection(image=image)
    labels = response.label_annotations

    if response.error.message:
        raise Exception(
            '{}\nFor more info on error messages, check: '
            'https://cloud.google.com/apis/design/errors'.format(
                response.error.message))

    descriptions = []
    for label in labels:
        descriptions.append(label.description)

    doc_ref = db.collection(u'images').document(filename)
    doc_ref.set({
        'labels': descriptions
    }, merge=True)

    return "Success"


if __name__ == "__main__":
    app.run(debug=True,
            port=int(os.environ.get('PORT')))
