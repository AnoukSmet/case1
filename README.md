# **YPA Case: Image Upload**

## Functionaliteiten
* HTTP GET: /images. toon je alle db entries van de geupload images als een lijst/gallerij. Op elke entry kan je klikken zodat je naar de detail pagina gaat. 
* HTTP POST: /images: uploaden van een nieuwe image + metadata. Image uploaden naar Cloud Storage. Metadata uploaden naar firestore.
* HTTP GET: /images/<UUID>: toont alle informatie die we hebben over 1 enkele image. /images/<UUID>
* HTTP GET: /images/<UUID>: indien UUID niet bekend is een melding tonen dat het er geen entry met UUID X bestaat.
* upload image body:
	1. naam
	2. upload date
	3. UUID genereren
	4. image
* gehost in je eigen GCP omgeving

### Step 1
1. bitbucket + GCP project aanmaken. Billing via Korf navragen.
2. python / flask
3. python type hinting (evt. via mypy static typing toepassen)
4. database mag zelf gekozen worden (voor nu is firestore nosql het makkelijkst)
5. indien je API bouwt dan REST/JSON
6. unit-test waar toepasbaar
7. app engine standard
8. Cloud Storage voor opslag images
9. Google Vision AI

### Step 2
1. app engine flexible
2. cloud run
3. docker container enabled
4. Cloud Tasks voor queue van images processing. Webhook of Cloud Run om het te processen (push of pull mechanisme)
 
#### Optioneel:
1. CI/CD
2. Authentication / Autorisation
3. Multi-tenant
4. Filtering en sortering
5. Memorystore voor caching
