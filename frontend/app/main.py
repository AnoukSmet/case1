import os
from app import app
from flask import render_template, redirect, request, url_for, flash, session
from werkzeug.utils import secure_filename
from app.cloud_storage import upload_image, delete_image_from_bucket
from app.cloud_firestore import upload_image_details, retrieve_image_details, retrieve_all_documents, delete_image_details, upload_user_info, check_existing_user, check_password, retrieve_searched_images
from google.cloud import tasks_v2, firestore
from werkzeug.security import generate_password_hash
import json
from app.models import Image, User
if os.path.exists('env.py'):
    import env

ALLOWED_EXTENSIONS = {'pdf', 'png', 'jpg', 'jpeg', 'gif'}


@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == "POST":
        if check_existing_user(request.form.get('username')):
            flash('Username already exists, please choose another one')
            return redirect(url_for('index'))

        if request.form.get('password') == request.form.get('repeat-password'):
            password = generate_password_hash(request.form.get('password') )
            user = User(username=request.form.get('username'), email=request.form.get('email'), password=password)
            # add user to firestore database
            upload_user_info(user)
            session['user'] = user.username
            flash('Congratulations, you are now a registered user!')
            return redirect(url_for('login'))
        else:
            flash("Your passwords don't match")
            return redirect(url_for('index'))

    return render_template('register.html', title='Register')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == "POST":
        if check_existing_user(request.form.get('username')):
            if check_password(request.form.get('username'), request.form.get('password')):
                session['user'] = request.form.get('username')
                flash('Welcome, {}'.format(request.form.get('username')))
                return redirect(url_for('index'))
            else:
                flash("password doesn't match")
                return redirect(url_for('login'))
        else:
            flash("username doesn't exist")
            return redirect(url_for('login'))

    return render_template('login.html', title='Sign In')


@app.route('/', methods=['GET', 'POST'])
def index():
    if session.get('user') is None:
        return redirect(url_for('login'))

    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            username = session.get('user')
            image = Image(file, filename, username)
            upload_image(image.file, image.uuid)
            upload_image_details(image.image_name, image.uuid, username)
            create_task(image.uuid)
            return redirect(url_for('display_image', image_id=image.uuid))
        else:
            flash('Allowed image types are - png, jpg, jpeg, gif')
    return render_template('index.html')


@app.route('/images', methods=['GET', 'POST'])
def view_images():
    if request.method == "POST":
        select_possibility = request.form["search-possibilities"]
        search_keyword = request.form["keyword"]
        user = session.get('user')

        images = retrieve_searched_images(search_keyword, select_possibility, user)

        print(images)

        return render_template('view-images.html', images=images)
    else:
        if session.get('user') is None:
            return redirect(url_for('login'))

        documents = retrieve_all_documents()
        images = []
        user = session.get('user')

        for doc in documents:
            if doc["username"] == user:
                images.append(doc["uuid"])

        search_possibilities = []
        for key in documents[0].keys():
            search_possibilities.append(key)

        return render_template('view-images.html', images=images, search_possibilities=search_possibilities)


@app.route('/images/<image_id>')
def display_image(image_id: str):
    doc = retrieve_image_details(image_id)
    if doc:
        image_data = doc.to_dict()
        image = "https://storage.cloud.google.com/ypa-image-upload.appspot.com/" + image_data["uuid"]
        return render_template('image-detail.html', image_data=image_data, image=image)
    else:
        flash('Image not found')
        return redirect(url_for('index'))


@app.route('/images/<image_id>/delete')
def delete_image(image_id: str):
    delete_image_details(image_id)
    delete_image_from_bucket(image_id)
    flash('Image and details deleted')
    return redirect(url_for('view_images'))


@app.route('/logout')
def logout():
    flash('You are now logged out')
    session.pop('user')
    return redirect(url_for('index'))


# Functions to check if file is allowed
def allowed_file(filename: str):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def create_task(image_id):
    client = tasks_v2.CloudTasksClient()
    project = 'ypa-image-upload'
    queue = 'image-upload-queue'
    location = 'europe-west3'
    url = 'https://backend-r3sga7jjma-ey.a.run.app/detect_labels_uri'

    # Construct the fully qualified queue name.
    parent = client.queue_path(project, location, queue)

    # Construct the request body.
    task = {
        "http_request": {  # Specify the type of request.
            "http_method": tasks_v2.HttpMethod.POST,
            "url": url,  # The full url path that the task will be sent to.
            'body': json.dumps({'filename': image_id}).encode()
        }
    }

    # Use the client to build and send the task.
    response = client.create_task(parent=parent, task=task)

    print('Created task {}'.format(response.name))
    return response


if __name__ == "__main__":
    app.run(debug=True,
            port=8000)
