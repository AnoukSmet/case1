from google.cloud import firestore
from datetime import datetime
from werkzeug.security import check_password_hash


db = firestore.Client()


# Upload details about image to Firestore
def upload_image_details(filename: str, image_id: str, username: str):
    doc_ref = db.collection(u'images').document(image_id)
    doc_ref.set({
        u'image_name': filename,
        u'upload_date': datetime.now(),
        u'uuid': image_id,
        u'username': username,
    })


# Upload User info to Firestore
def upload_user_info(user):
    doc_ref = db.collection(u'users').document(user.username)
    doc_ref.set({
        u'username': user.username,
        u'email': user.email,
        u'password': user.password,
    })


def check_existing_user(username):
    doc_ref = db.collection(u'users').document(username)
    doc = doc_ref.get()
    if doc.exists:
        return True
    else:
        return False

def check_password (username, password):
    doc_ref = db.collection(u'users').document(username)
    doc = doc_ref.get()
    if doc.exists:
        new_doc = doc.to_dict()
        return check_password_hash(new_doc["password"], password)


# Retrieve image details from specific image based on id
def retrieve_image_details(image_id: str) -> str:
    doc_ref = db.collection(u'images').document(image_id)
    doc = doc_ref.get()
    return doc


# Get all documents from Image collection
def retrieve_all_documents():
    docs = db.collection(u'images').stream()

    documents = []
    for doc in docs:
        documents.append(doc.to_dict())

    return documents


def retrieve_searched_images(search_keyword, select_possibility, user):
    images_ref = db.collection(u'images')
    query_ref = images_ref.where(select_possibility, u'==', search_keyword).where(u'username', u'==', user)

    images = []

    for doc in query_ref.stream():
        formatted_data = doc.to_dict()
        images.append(formatted_data["uuid"])

    return images



# Remove image details from specific image based on id
def delete_image_details(image_id: str):
    doc = db.collection(u'images').document(image_id)
    doc.delete()
    print("Image details deleted")
