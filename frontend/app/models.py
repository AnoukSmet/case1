from datetime import datetime
import uuid
from flask_login import UserMixin


class User(UserMixin):
    def __init__(self, username, email, password):
        self.id = uuid.uuid4().hex.upper()
        self.username = username
        self.email = email
        self.password = password

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Image:
    def __init__(self, file, filename, username):
        self.image_name = filename
        self.upload_date = datetime.now()
        self.uuid = uuid.uuid4().hex.upper()
        self.file = file
        self.username = username

