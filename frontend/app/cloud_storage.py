from google.cloud import storage


storage_client = storage.Client()
bucket_name = 'ypa-image-upload.appspot.com'


# Function to upload image to Cloud Storage Bucket
def upload_image(file: str, image_id):
    bucket = storage_client.bucket(bucket_name)
    image = bucket.blob(image_id)
    image.upload_from_file(
        file,
        content_type='image/jpg'
    )
    print(
        "File {} uploaded to {}.".format(
            file, image_id
        )
    )


# Function to delete image frm Cloud Storage Bucket
def delete_image_from_bucket(image_id: str):
    bucket = storage_client.bucket(bucket_name)
    image = bucket.blob(image_id)
    image.delete()
    print("Image deleted")
